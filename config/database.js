import { Sequelize } from "sequelize";

const db = new Sequelize('data_ft', 'root','',{
    host: process.env.DB_HOST,
    dialect: 'mysql'
});

export default db;