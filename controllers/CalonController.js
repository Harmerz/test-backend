import { Calon1 } from "../models/UsersModels.js";
import { Calon2 } from "../models/UsersModels.js";
import { User } from "../models/UsersModels.js";

export const createPemilih = async (req, res) => {
    try {

        const response = await User.findOne(
            {
                where: {
                    Niu: req.body.Niu
                },
            }
        );
        console.log(response?.dataValues)

        if(response !== null){
            if(String(req.body.paslon) === "1"){
                await Calon1.create(response?.dataValues);
                res.status(200).json({msg: "TERIMAKASIH SUDAH MEMILIH"});
            }else if(String(req.body.paslon) === "2"){
                await Calon2.create(response?.dataValues);
                res.status(200).json({msg: "TERIMAKASIH SUDAH MEMILIH"});
            }else{
                res.status(500).json({msg: "PASLON TIDAK TERDAFTAR"});
            }       
        }else {
            res.status(500).json({msg: "ANDA TIDAK TERDAFTAR"});
        }
         
    } catch (error) {
        console.log(error);
    }
}
