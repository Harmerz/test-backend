import {User} from "../models/UsersModels.js";

export const getUsers = async (req, res) => {
    try {
        // console.log(req, res);
        const response = await User.findAll();
        res.status(200).json(response);
    } catch (error) {
        console.log(error);
    }
}

export const getUsersByNIU = async (req, res) => {
    try {
        // console.log(req, res);
        const response = await User.findOne(
            {
                where: {
                    niu: req.params.niu
                },
            }
        );
        if(response==null){
            res.status(404).json({message: "Data not found"});
        }else{
           res.status(200).json(response);
            }
    } catch (error) {
        console.log(error);
    }
}

export const getUsersByEmail = async (req, res) => {
    try {
        const response = await User.findOne(
            {
                where: {
                    email: req.params.email
                },
            }
        );
        if(response==null){
            res.status(404).json({message: "Data not found"});
        }else{
           res.status(200).json(response);
            }
    } catch (error) {
        console.log(error);
    }
}

export const createUser = async (req, res) => {
    try {
        if (String(req.body.password) === "qweasd123qweasdzxc"){
            await User.create(req.body);
            res.status(201).json({msg: "USER CREATED"});
        } else {
            res.status(404).json({msg: "WRONG PASSWORD"});
        }
    } catch (error) {
        console.log(error);
    }
}

export const updateUser = async (req, res) => {
    try {
        
        if (String(req.body.password) === "qweasd123qweasdzxc"){
            await User.update(req.body, {
                where: {
                    Niu: req.body.Niu
                }
            });
            res.status(201).json({msg: "USER UPDATE"});
        } else {
            res.status(404).json({msg: "WRONG PASSWORD"});
        }
    } catch (error) {
        console.log(error);
    }
}