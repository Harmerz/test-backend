import express from "express";
import cors from "cors";
import db from "./config/database.js";
import UserRoute from "./routes/UserRoute.js";

var corsOptions = {
    origin: 'https://example.com',
    optionsSuccessStatus: 20011 // some legacy browsers (IE11, various SmartTVs) choke on 204
  }

try {
    await db.authenticate();
    console.log('Database connected...');
} catch (error) {
    console.error('Connection error:', error);
}
 

const app = express();
app.use(cors(corsOptions));
app.use(express.json());
app.use(UserRoute);



const PORT = process.env.PORT || 5000;

app.listen(PORT, () =>   console.log(`Server started on port ${PORT}`));