import { Sequelize } from "sequelize";
import db from "../config/database.js";
import csvtojson from "csvtojson";

const {DataTypes} = Sequelize;

export const User = db.define('users', {
    Nim: DataTypes.STRING,
    Niu: DataTypes.STRING,
    Jurusan: DataTypes.STRING,
    Name: DataTypes.STRING,
    Email: DataTypes.STRING,
},{
    freezeTableName: true
})

export const Login = db.define('login', {
    Name: DataTypes.STRING,
    Email: DataTypes.STRING,
    Token: DataTypes.STRING,
},{
    freezeTableName: true
})

export const Pemilih = db.define('pemilih', {
    Nim: DataTypes.STRING,
    Niu: DataTypes.STRING,
    Jurusan: DataTypes.STRING,
    Name: DataTypes.STRING,
    Email: DataTypes.STRING,
},{
    freezeTableName: true
})

export const Calon1 = db.define('calon1', {
    Nim: DataTypes.STRING,
    Niu: DataTypes.STRING,
    Jurusan: DataTypes.STRING,
    Name: DataTypes.STRING,
    Email: DataTypes.STRING,
},{
    freezeTableName: true
})

export const Calon2 = db.define('calon2', {
    Nim: DataTypes.STRING,
    Niu: DataTypes.STRING,
    Jurusan: DataTypes.STRING,
    Name: DataTypes.STRING,
    Email: DataTypes.STRING,
},{
    freezeTableName: true
})

export const Departemen = db.define('departemen', {
    DTAP: DataTypes.STRING,
    DTK: DataTypes.STRING,
    DTETI: DataTypes.STRING,
    DTMI: DataTypes.STRING,
    DTNTF: DataTypes.STRING,
    DTSL: DataTypes.STRING,
    DTGD: DataTypes.STRING,
    DTGL: DataTypes.STRING,
},{
    freezeTableName: true
})


const fileName = "data.csv";
  


(async () => {
    await db.sync({force: true});
    csvtojson().fromFile(fileName).then(async source => {
  
        // Fetching the data from each row 
        // and inserting to the table "sample"
        for (var i = 0; i < source.length; i++) {
            var Nim = source[i]["NIM"],
                Jurusan = source[i]["PROGRAM STUDI"],
                Nama = source[i]["NAMA"],
                Email = source[i]["MAIL"]
            var dataSplit = Nim.split("/");
            var Niu = dataSplit[1];
            // Inserting the data into the table
            if(Email !== ""){
                await User.create({
                    Nim: Nim,
                    Niu: Niu,
                    Jurusan: Jurusan,
                    Name: Nama,
                    Email: Email
                });
            }
        }
        console.log(
    "All items stored into database successfully");
    });
})();
