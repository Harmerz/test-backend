import express from "express";
import { createPemilih } from "../controllers/CalonController.js";
import {createUser, getUsers, getUsersByEmail, getUsersByNIU, updateUser} from "../controllers/UserController.js"; 

const router = express.Router();


router.get('/users', getUsers);
router.get('/users/:niu', getUsersByNIU);
router.get('/users/check/:email', getUsersByEmail);
router.post('/users', createUser);
router.put('/users', updateUser);


router.post('/pemilih', createPemilih);

export default router;